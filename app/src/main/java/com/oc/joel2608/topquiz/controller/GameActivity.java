package com.oc.joel2608.topquiz.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.oc.joel2608.topquiz.R;


public class GameActivity extends AppCompatActivity {

    private TextView mQuestionText;
    private Button mAnswerOne;
    private Button mAnswerTwo;
    private Button mAnswerThree;
    private Button mAnswerFour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mQuestionText = (TextView) findViewById(R.id.activity_game_question_text);
        mAnswerOne = (Button) findViewById(R.id.activity_game_answer1_btn);
        mAnswerOne = (Button) findViewById(R.id.activity_game_answer2_btn);
        mAnswerOne = (Button) findViewById(R.id.activity_game_answer3_btn);
        mAnswerOne = (Button) findViewById(R.id.activity_game_answer4_btn);
    }
}
